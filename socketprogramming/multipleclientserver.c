//Handle multiple socket connections with select and fd_set on Linux
#include <stdio.h>
#include <string.h>   //strlen
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>   //close
#include <arpa/inet.h>    //close
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros

#define PORT 1234
#define max_clients 30
pthread_t listenerthread;
pthread_t readerthread;
pthread_attr_t attr;

int sock_server;
struct sockaddr_in address;
char buffer[1025];  //data buffer of 1K
fd_set readfds;     //set of socket descriptors
int client_socket[max_clients];
int new_socket;     //new socket incoming.

void listener(void * arg);
void reader(void *arg);


int main(int argc, int argv[])
{
  int optval = 1,i;
  for (i = 0; i < max_clients; i++)
  {
      client_socket[i] = 0;
  }
// creating socket and saving its id to sockint
  sock_server = socket(AF_INET, SOCK_STREAM, 0);
  if(sock_server < 0)
  {
    perror("Socket error : ");
    exit(EXIT_FAILURE);
  }
//setting options for sock_server
  if( setsockopt(sock_server, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) < 0 )
  {
    perror("Socket option error : ");
    exit(EXIT_FAILURE);
  }
//fillind server_address structure with address port and family type(IPv4)
  address.sin_family =  AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = ntohs(PORT);
//binding socket to the ip address; typecastig server_address to sockaddr type.
  if( bind(sock_server, (struct sockaddr *)&address, sizeof(address)) < 0 )
  {
    perror("Bind error : ");
    exit(EXIT_FAILURE);
  }

  printf("listener on port %d\n", PORT);

  if(listen(sock_server, 5))
  {
    perror("Listen error : ");
    exit(EXIT_FAILURE);
  }

listener(NULL);
//    pthread_create(&listenerthread, &attr, listener, NULL);

    //  pthread_create(&readerthread, &attr, reader, NULL);
//  pthread_join();

}




void listener(void * arg){
  int max_sd,i,sd,activity,addrlen;
  char *message = "Greetings from the Server\r\n";
  while (1) {
    addrlen = sizeof(address);
    // clearing activities of file descriptor
    FD_ZERO(&readfds);
    //setting sock_server socket to file descriptor to look for  any incoming connections
    FD_SET(sock_server, &readfds);
    //add child sockets to set
    for ( i = 0 ; i < max_clients ; i++){
        //socket descriptor
        sd = client_socket[i];
        //if valid socket descriptor then add to read list
        if(sd > 0)
            FD_SET( sd , &readfds);
        //highest file descriptor number, need it for the select function
        if(sd > max_sd)
            max_sd = sd;
    }
    //wait for an activity on one of the sockets , timeout is NULL ,
    //so wait indefinitely
    activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);
    if ((activity < 0) && (errno!=EINTR)){
        printf("Select error\n");
    }
    //If something happened on the master socket ,
    //then its an incoming connection
    if (FD_ISSET(sock_server, &readfds)){
      new_socket = accept(sock_server,(struct sockaddr *)&address, (socklen_t*)&addrlen);
      if (new_socket<0){
        perror("Accept :");
        exit(EXIT_FAILURE);
      }
      //inform user of socket number - used in send and receive commands
      printf("New connection , socket fd is %d , ip is : %s , port : %d\n" , new_socket , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));
      //send new connection greeting message
      if( send(new_socket, message, strlen(message), 0) != strlen(message) ){
        perror("Send :");
        exit(EXIT_FAILURE);
      }
      printf("Welcome message sent successfully\n");
      //add new socket to array of sockets
      for (i = 0; i < max_clients; i++){
          //if position is empty
          if( client_socket[i] == 0 ){
              client_socket[i] = new_socket;
              printf("Adding to list of sockets as %d\n" , i);
              break;
          }
      }
    }
    reader(NULL);
  }

}



void reader(void *arg){
  int i,sd,valread,addrlen;
  //else its some IO operation on some other socket
  for (i = 0; i < max_clients; i++){
    sd = client_socket[i];
    if (FD_ISSET( sd , &readfds))
      {
        //Check if it was for closing , and also read the
        //incoming message
        if ((valread = read( sd , buffer, 1024)) == 0){
            //Somebody disconnected , get his details and print
            getpeername(sd , (struct sockaddr*)&address,(socklen_t*)&addrlen);
            printf("Host disconnected , ip %s , port %d \n",inet_ntoa(address.sin_addr) , ntohs(address.sin_port));
            //Close the socket and mark as 0 in list for reuse
            close( sd );
            client_socket[i] = 0;
        }
       //Echo back the message that came in
       else{
         //set the string terminating NULL byte on the end
         //of the data read
         buffer[valread] = '\0';
         send(sd , buffer , strlen(buffer) , 0 );
         }
      }
   }
}
