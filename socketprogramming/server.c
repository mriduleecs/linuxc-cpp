#include"stdio.h"
#include"stdlib.h"
#include"sys/types.h"
#include"sys/socket.h"
#include"string.h"
#include"netinet/in.h"
#include"pthread.h"

#define PORT 4444
#define BUF_SIZE 2000
#define CLADDR_LEN 100
char clientAddr[CLADDR_LEN];
char syshost[257];
int sockfd, len, ret, newsockfd;

void * connectionaccept(void * arg)
{
  struct sockaddr_in * argm = (struct sockaddr_in*) arg;

//  while(1)
  {

    inet_ntop(AF_INET, &(argm->sin_addr), clientAddr, CLADDR_LEN);
    printf("Connection accepted from %s...\n", clientAddr);

  }
}

void * receiveMessage(void * socket) {
 int sockfd, ret;
 char buffer[BUF_SIZE];
 sockfd = (int) socket;
 memset(buffer, 0, BUF_SIZE);
 for (;;) {
  ret = recvfrom(sockfd, buffer, BUF_SIZE, 0, NULL, NULL);
  if (ret < 0) {
   printf("Error receiving data!\n");
  } else {
   printf("client: ");
   fputs(buffer, stdout);
   //printf("\n");
  }
 }
}

void main() {
 struct sockaddr_in addr, cl_addr;
 char buffer[BUF_SIZE];
 pid_t childpid;
 pthread_t rThread;
 pthread_t rThread2;

 gethostname(syshost,256);
 printf("\r\nName of System : %s \r\n",syshost);
 sockfd = socket(AF_INET, SOCK_STREAM, 0);                    //sockfd is return to socket() function
 if (sockfd < 0) {
  printf("Error creating socket!\n");
  exit(1);
 }
 printf("Socket created...\n");

 memset(&addr, 0, sizeof(addr));
 addr.sin_family = AF_INET;
 addr.sin_addr.s_addr = INADDR_ANY;
 addr.sin_port = PORT;

 ret = bind(sockfd, (struct sockaddr *) &addr, sizeof(addr)); //bind the socket to a local address
 if (ret < 0) {
  printf("Error binding!\n");
  exit(1);
 }
 printf("Binding done...\n");

 printf("Waiting for a connection...\n");
listen(sockfd, 5);
len = sizeof(cl_addr);
newsockfd = accept(sockfd, (struct sockaddr *) &cl_addr, &len);
if (newsockfd < 0) {
 printf("Error accepting connection!\n");
 exit(1);
}



 //thread for accepting connections

ret = pthread_create(&rThread2, NULL, connectionaccept, &cl_addr);
if (ret) {
 printf("ERROR: Return Code from pthread_create() is %d\n", ret);
 exit(1);
}

 memset(buffer, 0, BUF_SIZE);
 printf("Enter your messages one by one and press return key!\n");

 //creating a new thread for receiving messages from the client
 ret = pthread_create(&rThread, NULL, receiveMessage, (void *) newsockfd);
 if (ret) {
  printf("ERROR: Return Code from pthread_create() is %d\n", ret);
  exit(1);
 }

 while (fgets(buffer, BUF_SIZE, stdin) != NULL) {
  ret = sendto(newsockfd, buffer, BUF_SIZE, 0, (struct sockaddr *) &cl_addr, len);
  if (ret < 0) {
   printf("Error sending data!\n");
   exit(1);
  }
 }

 close(newsockfd);
 close(sockfd);

 pthread_exit(NULL);
 return;
}
