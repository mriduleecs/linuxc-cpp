#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "errno.h"
#include "unistd.h"
#include "arpa/inet.h"
#include "sys/types.h"
#include "sys/socket.h"
#include "netinet/in.h"
#include "sys/time.h"
#include "pthread.h"
#include "poll.h"


#define PORT 1234
#define max_threads 100
#define thread_timeout_sec  20

pthread_t reader_writer;
pthread_attr_t attr;
pthread_mutex_t rw_lock;
void *ServerEcho(void *args);

struct client_desc{
  int clientDescriptor;
  int socketDescriptor; /*  For file descriptor check and timeout   */
  char client_ipaddress[50];
}client[max_threads];

int main(int argc, char const *argv[]) {
  fd_set readfds;     //set of socket descriptors
  struct sockaddr_in sock_var;
  struct sockaddr_in client_var;
  int serverFileDescriptor;
  int clientFileDescriptor;
  int addrlen = sizeof(client_var);
  FD_ZERO(&readfds);
  FD_SET(serverFileDescriptor, &readfds);
//  FD_SET(clientFileDescriptor, &readfds);

  pthread_mutex_init ( &rw_lock, NULL);
  serverFileDescriptor = socket(AF_INET, SOCK_STREAM, 0);
  perror("[*] Socket creation ");
  sock_var.sin_addr.s_addr = INADDR_ANY;
  sock_var.sin_port = ntohs(PORT);
  sock_var.sin_family = AF_INET;

  if(bind(serverFileDescriptor, (struct sockaddr*)&sock_var, sizeof(sock_var)) >= 0){
    printf("%s\n", "[*] Socket has been created;");
    listen(serverFileDescriptor, 0);

    while(1){
//      for(i=0;i<max_threads;i++){
        clientFileDescriptor = accept(serverFileDescriptor, (struct sockaddr *)&client_var,  (socklen_t*)&addrlen);
//        clientFileDescriptor = 1;
        printf("%d\n", clientFileDescriptor);
        memset(&client[clientFileDescriptor], 0, sizeof(client[clientFileDescriptor]));/* Clearing the client structure*/
        client[clientFileDescriptor].clientDescriptor = clientFileDescriptor;
        client[clientFileDescriptor].socketDescriptor = serverFileDescriptor;
        strcat(client[clientFileDescriptor].client_ipaddress, inet_ntoa(client_var.sin_addr));
        printf("Thread status : %d ",pthread_create(&reader_writer,  &attr, ServerEcho, &client[clientFileDescriptor]));
//        printf("Thread status : %d ",pthread_create(&reader_writer[clientFileDescriptor],  &attr, ServerEcho, &client[clientFileDescriptor]));
//        pthread_join(reader_writer[clientFileDescriptor], NULL);
//      }
    }
    close(serverFileDescriptor);
  }else{
    perror("[*] Socket Creation Failed");
  }
  return 0;
}

void *ServerEcho(void *args){
  struct pollfd fd;   /* For checking any event on filedescriptor till desired timeouts*/
  char incoming_message[100];
  char outgoing_message[] = {"[#] Received by the Server;\n"};
//  struct sockaddr_in client_var;
//  pthread_mutex_lock(&rw_lock);
  struct client_desc *client = (struct client_desc*)args;
  printf("%s%d%s%s\n%s\n", "[*] Connected to client ",client->clientDescriptor, \
   " with ip address : ", client->client_ipaddress, "[*] Waiting from a message from the Client");
  fd.fd = client->clientDescriptor;
  fd.events = POLLIN;
  switch(poll(&fd, 1, (thread_timeout_sec*1000))){    /*  Ten Seconds timeout */
    case  -1:
      break;
    case  0:
      close(client->clientDescriptor);
      printf("%s : %d;\n", "[*] Timout Occured for the client", client->clientDescriptor);
      perror("Client closing status");
      break;  /*  timeout   */
    default:
      read(client->clientDescriptor, incoming_message, 100);
      printf("\n%s\n", "[*] Reading from the client;");
      printf("%s : %s\n", "[*] Incoming Message", incoming_message);
      write(client->clientDescriptor, outgoing_message, sizeof(outgoing_message));
      close(client->clientDescriptor);
      printf("%s : %d;\n", "[*] Achnowledged the client", client->clientDescriptor);
      perror("Client closing status");
  }
//  pthread_mutex_unlock(&rw_lock);
  pthread_exit(NULL);
}
