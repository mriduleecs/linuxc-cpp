#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

long long sum;


void * sumrunner(void * arg)
{
  long long * limitptr = (long long *) arg;
  long long limit = *limitptr;
  for (long long i = 0; i <= limit; i++)
  {
    sum += i;
  }
}

void main(int argc, char *argv[]) {
  /* code */
  pthread_t tid;
  pthread_attr_t attr;
  long long limit1 = atoll(argv[1]);
  pthread_attr_init(&attr);
  pthread_create(&tid, &attr, sumrunner, &limit1);
  pthread_join(tid, NULL);
  printf("\r\nPID=%ld,SUM=%lld\r\n",tid,sum);
}
