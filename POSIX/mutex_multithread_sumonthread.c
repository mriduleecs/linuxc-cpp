#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

pthread_mutex_t mutex;


long long sum;
struct sumrunner_struct
{
  int id;
  long long limit;
  long long sum;
};


void * sumrunner(void * argm)
{
  pthread_mutex_lock(&mutex);
  struct sumrunner_struct * argsumrunner = (struct sumrunner_struct *) argm;
  for (long long i = 0; i <= argsumrunner->limit; i++)
  {
    argsumrunner->sum += i;
  }
  pthread_mutex_unlock(&mutex);
 printf("\r\nPID=%d,LIMIT=%lld,SUM=%lld\r\n",argsumrunner->id,argsumrunner->limit,argsumrunner->sum);
}

void main(int argc, char *argv[]) {
  int threads = argc-1;
  struct sumrunner_struct arg[threads];
  pthread_t tid[threads];
  pthread_attr_t attr;

  pthread_attr_init(&attr);

  for(int i = 0; i < threads; i++)
  {
    arg[i].limit = atoll(argv[i+1]);
    arg[i].sum = 0;
    arg[i].id = i;
    printf("%d,%lld,%lld\n",arg[i].id,arg[i].limit,arg[i].sum);
//  printf("\n%lld",arg[i].limit);
    pthread_create(&tid[i], &attr, sumrunner, &arg[i]);
  }
  for(int i = 0; i < threads; i++)
  {
    pthread_join(tid[i], NULL);
  }




}
