#include <stdio.h>
#include <pthread.h>

void * demo(void * arg)
{
  printf("Hello World\r\n");
}

int main()
{
  pthread_t tid;
  pthread_create(&tid, NULL, demo, NULL);
  pthread_join(tid,NULL);
  return 0;
}
